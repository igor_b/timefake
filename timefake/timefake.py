#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import logging
import time
import sys

"""
timefake module adds possibility to patch current date/time in time and datetime
modules to a predefined (local) date/time. Time flow will continue normally after
that.

patch_from_cmdline() function will search for --set-fake-time command line argument
in YYYY-mm-ddTHH:MM:SS or 'YYYY-mm-dd HH:MM:SS' format (local time). Any "shorter"
representation (without seconds, without minute and seconds, ...) is also valid.
"""

# ------------------ time module methods ----------------
#
# Explanation for *args:
# When monkey patched method is assigned to a class, it becomes bound method for
# object of that class and when called the first argument is class' self.
#
# To work around that, following  methods that accept some arguments are
# prepared for both cases.
#

def _mytime(offset=0):
    if not hasattr(_mytime, 'time'):
        _mytime.time = time.time
        _mytime.offset = offset
    return _mytime.time()-_mytime.offset

def _mylocaltime(*args):
    if not hasattr(_mylocaltime, 'base'):
        _mylocaltime.base = time.localtime
    if len(args)==1:
        t=args[0]
    elif len(args)==2:
        t=args[1]
    else:
        t = time.time()
    return _mylocaltime.base(t)

def _mygmtime(*args):
    if not hasattr(_mygmtime, 'base'):
        _mygmtime.base = time.gmtime
    if len(args)==1:
        t=args[0]
    elif len(args)==2:
        t=args[1]
    else:
        t = time.time()
    return _mygmtime.base(t)

def _myasctime(*args):
    if not hasattr(_myasctime, 'base'):
        _myasctime.base = time.asctime
    if len(args)==1:
        t=args[0]
    elif len(args)==2:
        t=args[1]
    else:
        t = time.localtime(_mytime())
    return _myasctime.base(t)

def _myctime(*args):
    if not hasattr(_myctime, 'base'):
        _myctime.base = time.ctime
    if len(args)==1:
        t=args[0]
    elif len(args)==2:
        t=args[1]
    else:
        t = time.time()
    return _myctime.base(t)


# ------------------ datetime module methods ----------------

class _mydatetime(datetime.datetime):
    @classmethod
    def now(cls, tz=None):
        "Construct a datetime from time.time() and optional time zone info."
        t = time.time()
        return cls.fromtimestamp(t, tz)

    @classmethod
    def utcnow(cls):
        "Construct a UTC datetime from time.time()."
        t = time.time()
        return cls.utcfromtimestamp(t)

class _mydate(datetime.date):
    @classmethod
    def today(cls):
        "Construct a date from time.time()."
        t = _time.time()
        return cls.fromtimestamp(t)


# ------------------ patcher ----------------

def patch(to_date):
    """
    will patch time and datetime modules to start at the date/time proovided
    as time tuple
    """
    # convert wanted time tuple to timestamp and calculate the offset
    to_ts = time.mktime(to_date)
    offset = time.time()-to_ts

    # initialize time functions
    _mytime(offset)
    _mylocaltime()
    _mygmtime()
    _myasctime()
    _myctime()

    # patch time module
    time.time = _mytime
    time.localtime = _mylocaltime
    time.gmtime = _mygmtime
    time.asctime = _myasctime
    time.ctime = _myctime

    # patch datetime module
    datetime.datetime = _mydatetime
    datetime.date = _mydate

def patch_from_cmdline(args=None):
    """
    will search for --set-fake-time command line argument in
    YYYY-mm-ddTHH:MM:SS or 'YYYY-mm-dd HH:MM:SS' format (local time) and
    patch the date/time to that point in time.
    """

    if args is None:
        args = sys.argv
    try:
        dt = args[args.index('--set-fake-time')+1]
    except (ValueError, IndexError, ):
        # do nothing
        return

    formats = (
            '%Y-%m-%d %H:%M:%S',
            '%Y-%m-%dT%H:%M:%S',
            '%Y-%m-%d %H:%M',
            '%Y-%m-%dT%H:%M',
            '%Y-%m-%d %H',
            '%Y-%m-%dT%H',
            '%Y-%m-%d',
            )
    for f in formats:
        try:
            tt = time.strptime(dt, f)
            break
        except ValueError:
            pass
    else:
        logging.error("Invalid (fake) date/time specified on command line")
        return

    patch(tt)

